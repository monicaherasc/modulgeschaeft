package awacademy.store.services;

import awacademy.store.dtos.AdminDTO;
import awacademy.store.dtos.OfferDTO;

import awacademy.store.entities.OfferEntity;
import awacademy.store.respositories.OfferRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfferServcieTest {
    @Autowired
    private OfferRepository offerRepository;

//    @Test
//    public void createOfferTest(){
//
//    OfferService offerService = new OfferService(offerRepository);
//    OfferDTO offerDTO = new OfferDTO();
//    offerDTO.setTitle("Large Pizza");
//    offerService.createOffer(offerDTO);
//
//    OfferEntity offerEntity = offerRepository.findByTitle("Large Pizza");
//    Assert.assertEquals(offerEntity.getTitle(), offerDTO.getTitle());
//
//    }

    @Test
    public void readOfferTest(){
        OfferEntity offerEntity = new OfferEntity();
        OfferEntity offerEntity1 = new OfferEntity();


        offerRepository.save(offerEntity);
        offerRepository.save(offerEntity1);

        OfferService offerService = new OfferService(offerRepository);

        List<OfferDTO> lista = offerService.readAllOffer();
        System.out.println(lista.size());

        Assert.assertEquals(lista.size(), 2);

    }






}
