package awacademy.store.services;

import awacademy.store.dtos.BusinessDTO;
import awacademy.store.entities.BusinessEntity;
import awacademy.store.respositories.BusinessRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@DataJpaTest
public class BusinessServiceTest {
    @Autowired
    private BusinessRepository businessRepository;

    @Test
    public void addAStore(){
        BusinessDTO businessDto = new BusinessDTO();
        businessDto.setBusinessName("Riku");
        businessDto.setPassword("89");
        BusinessService companyService = new BusinessService(businessRepository);
        companyService.createNewBusiness(businessDto);


        BusinessEntity riku = businessRepository.findByBusinessName("Riku");
        Assert.assertEquals(riku.getBusinessName(), businessDto.getBusinessName());

    }

    @Test
    public void readAllCompaniesTest(){
        BusinessEntity companyEntity = new BusinessEntity();
        BusinessEntity companyEntity1 = new BusinessEntity();

        businessRepository.save(companyEntity);
        businessRepository.save(companyEntity1);

        BusinessService companyService = new BusinessService(businessRepository);

        List<BusinessDTO> lista = companyService.readAllBusiness();
        System.out.println(lista.size());

        Assert.assertEquals(lista.size(), 2);

    }

    @Test
    public void deleteAStore() {
        BusinessEntity companyEntity = new BusinessEntity();
        BusinessEntity companyEntity1 = new BusinessEntity();

        businessRepository.save(companyEntity);
        businessRepository.save(companyEntity1);
        Long id = companyEntity.getId();

        BusinessService companyService = new BusinessService(businessRepository);
        List<BusinessEntity> listPreDelete = businessRepository.findAll();
        companyService.delete(id);;

        List<BusinessEntity> listPostDelete = businessRepository.findAll();
        System.out.println(listPreDelete.size());
        System.out.println(listPostDelete.size());

        Assert.assertEquals(listPreDelete.size(), listPostDelete.size()+1);

    }

    @Test

    public void updateTest (){

        BusinessEntity business1 = new BusinessEntity();
        business1.setBusinessName("IceCream");
        business1.setPassword("89");
        businessRepository.save(business1);

        System.out.println(business1.getId());

        BusinessDTO businessDTOUpdtae = new BusinessDTO();
        businessDTOUpdtae.setBusinessName("Titos");
        BusinessService companyService = new BusinessService(businessRepository);
        companyService.update(businessDTOUpdtae,business1.getId());
        BusinessEntity businessUpdated = businessRepository.findById(business1.getId()).get();


        Assert.assertEquals(businessUpdated.getBusinessName(), "Titos");

    }


}
