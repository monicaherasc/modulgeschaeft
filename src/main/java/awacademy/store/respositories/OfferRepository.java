package awacademy.store.respositories;


import awacademy.store.entities.OfferEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OfferRepository extends JpaRepository<OfferEntity,Long> {
    OfferEntity findByTitle(String title);

    @Query("select o from OfferEntity o where o.code = :code")
    OfferEntity findOrderByCode(String code);



}
