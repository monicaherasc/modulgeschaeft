package awacademy.store.Controller;


import awacademy.store.dtos.ess.EssOrderDTO;
import awacademy.store.dtos.OrderDTO;
import awacademy.store.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/order")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }


    @PostMapping("/createOrder")
    public String postOrder(@RequestBody EssOrderDTO essOrderDTO){

        orderService.createOrder(essOrderDTO);

        return "Order success";

    }

    @GetMapping("/getOrder")
    public List<OrderDTO> readAllOrders(){
        return orderService.readAllOrders();
    }
}
