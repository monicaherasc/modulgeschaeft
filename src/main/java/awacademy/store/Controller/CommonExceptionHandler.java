package awacademy.store.Controller;

import awacademy.store.dtos.ErrorDTO;
import awacademy.store.exceptions.BusinessNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**Wir schreiben unsere ExceptionHandler als seperate Klasse zuständig für unsere Controller
 * (Handler können auch direkt in die Controller
 * implementiert werden aber good practice = seperate ExceptionKlasse)
 *
 * @RestControllerAdvice stellt den zusätzlichen Inhalt dieser Klasse,
 * jedem annotierten Controller zur Verfügung!
 * Die Logik in dieser Klasse landet später ebenfalls bei jedem Controller.
 *
 * Achtung: Fachlich handelt es sich hier bei dieser KLasse um einen ExceptionHandler,
 * damit die Spring Pfade korrekt funktionieren
 * sollte die Klasse allerdings nicht konkret "ExceptionHandler" heißen.
 * In diesem Fall "CommonExceptionHandler"
 */
@RestControllerAdvice
public class CommonExceptionHandler {

    @ExceptionHandler()
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDTO handleExceptions(BusinessNotFoundException e){
        return new ErrorDTO(e.getMessage());
    }


}
