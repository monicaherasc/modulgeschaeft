package awacademy.store.Controller;


import awacademy.store.dtos.BusinessDTO;
import awacademy.store.dtos.OfferDTO;
import awacademy.store.dtos.OfferToSendDTO;
import awacademy.store.entities.BusinessEntity;
import awacademy.store.entities.OfferEntity;
import awacademy.store.services.ExternalServiceService;
import awacademy.store.services.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200/"})
@RestController
@RequestMapping("/offer") //creates url
public class OfferController {


    private OfferService offerService;
    private ExternalServiceService externalServiceService;

    @Autowired
    public OfferController(OfferService offerService, ExternalServiceService externalServiceService) {
        this.offerService = offerService;
        this.externalServiceService = externalServiceService;
    }

    @PostMapping("/postoffer")
    @ResponseStatus (HttpStatus.CREATED)
    public void createOffer(@RequestBody OfferToSendDTO offerToSendDTO){
        // @RequestBody maps our JSON object to an OfferDTO

        OfferToSendDTO offerTosafe = externalServiceService.offerRegistration(offerToSendDTO);
        offerService.createOffer(offerTosafe);
    }

    @GetMapping("/getalloffers")
    public List<OfferDTO> readAll(){
        return offerService.readAllOffer();
    }




    //ToDO - Delete and update NOT MVP

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        offerService.delete(id);
    }


    @PutMapping("/update/{id}")
    @ResponseStatus (HttpStatus.CREATED)
    public OfferEntity update (@RequestBody OfferDTO offerDTO, @PathVariable Long id){

       OfferEntity offerActual = offerService.update(offerDTO,id);
        return offerService.save(offerActual);
    }


}
