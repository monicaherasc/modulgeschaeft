package awacademy.store.Controller;

import awacademy.store.dtos.AdminDTO;
import awacademy.store.dtos.TimeDTO;
import awacademy.store.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
@RestController
@RequestMapping("/admin")
public class AdminController {


    private AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    /**
     * Admin gives us a request with a TimeDTO (gives us two timestamps)
     * this method uses the given dto for a query and returns a list of businesses for the
     * time period
     * @param timeDTO contains two timestamps for a query
     * @return a list of businesses for the requested timeperiod
     */
    @PostMapping("/allBusiness")
    public List<AdminDTO> giveCompanies(@RequestBody TimeDTO timeDTO)  {

        return adminService.readAllCompanies(timeDTO.getStartDate(), timeDTO.getEndDate());

    }

}
