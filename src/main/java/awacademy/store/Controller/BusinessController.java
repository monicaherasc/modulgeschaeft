package awacademy.store.Controller;

import awacademy.store.dtos.BusinessDTO;
import awacademy.store.exceptions.BusinessNotFoundException;
import awacademy.store.services.BusinessService;
import awacademy.store.services.ExternalServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/business")
public class BusinessController {


    private final BusinessService businessService;
    private final ExternalServiceService externalServiceService;

    @Autowired
    public BusinessController(BusinessService businessService, ExternalServiceService externalServiceService) {
        this.businessService = businessService;
        this.externalServiceService = externalServiceService;
    }


    @PostMapping("/creation")
    public void createBusiness(@RequestBody BusinessDTO businessDto){


        // Antwort aus ESS gibt uns einen Code, den wir wiederum in unserem DTO setten und abspeichern (dann als Entität)
        String code  = externalServiceService.businessRegistration(businessDto.getId());

        businessDto.setQrCode(code);
        businessService.createNewBusiness(businessDto);

    }

    @GetMapping("/allBusiness")
    public List<BusinessDTO> readAllBusiness() {

        if (businessService.readAllBusiness() == null || businessService.readAllBusiness().isEmpty()) {
            throw new BusinessNotFoundException("Error: No Business found!");
        }

        return businessService.readAllBusiness();
    }

//    // ExceptionHandler fängt unsere Exceptions hier auf
//      Siehe seperate Klasse "CommmonExceptionHandler"

//    @ExceptionHandler(value = BusinessNotFoundException.class)
//    public ErrorDTO handleNotFoundException(BusinessNotFoundException e) {
//        return new ErrorDTO (e.getMessage());
//    }



}
