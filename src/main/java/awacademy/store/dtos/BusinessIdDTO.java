package awacademy.store.dtos;

import lombok.Data;


/**
 * This DTO only exists bc JSON Format expects objects, otherwise we would just send the businessID to se boys
 */
@Data
public class BusinessIdDTO {

    private Long businessID;
}
