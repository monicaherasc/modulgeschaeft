package awacademy.store.dtos;

import lombok.Data;

import java.util.UUID;

@Data
public class ErrorDTO {

    private final String message;

    public ErrorDTO(String message) {
        this.message = message;
    }



}
