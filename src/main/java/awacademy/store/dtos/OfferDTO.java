package awacademy.store.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


@Data
public class OfferDTO {

    private String title;
    private String price;
    private String about;



    // Generierter QR-String-Code (Mit O)

    private String code;

    // This stays empty until mapping(!)
    @JsonIgnore
    private Long id;


}
