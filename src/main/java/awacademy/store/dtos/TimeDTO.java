package awacademy.store.dtos;

import lombok.Data;

@Data
public class TimeDTO {

    private String startDate;
    private String endDate;

}
