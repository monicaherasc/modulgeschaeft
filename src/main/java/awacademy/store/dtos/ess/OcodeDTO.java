package awacademy.store.dtos.ess;

import lombok.Data;

@Data
public class OcodeDTO {

    private String code;
    //BusinessID
    private Long id;
}
