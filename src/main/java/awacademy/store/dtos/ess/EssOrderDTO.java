package awacademy.store.dtos.ess;

import lombok.Data;

@Data
public class EssOrderDTO {

    private String customerID;
    private String code;
}
