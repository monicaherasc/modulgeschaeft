package awacademy.store.dtos;


import lombok.Data;



@Data
public class OrderDTO {


    private String title;
    private String description;
    private String dateOfOrder;

    // Stays empty until mapping(!) and can be used as a orderNumber for the frontend or whatever
    private Long id;

    private Long customerID;



}
