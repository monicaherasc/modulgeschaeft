package awacademy.store.dtos;

import lombok.Data;


@Data
public class BusinessDTO {

    private String businessName;
    private String password;
    private String street;
    private String postcode;
    private String city;
    private String country;
    private String phoneNumber;
    // email = username in auth
    private String email;
    private String branche;
    private boolean onlinePayment;


    private String qrCode;
    private Long id;
}
