package awacademy.store.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class OfferToSendDTO {

    private String title;
    private String price;
    private String about;

    private Long businessId;

    // QR-Code wird erst beim Erstellen im Service zugewiesen
    @JsonIgnore
    private String code;
}
