package awacademy.store.dtos;

import lombok.Data;

@Data
public class CompanyDto {
    private String name;
    private String password;
    private String address;
    private String phoneNumber;
    private String mail;
    private String category;
    private boolean onlinePayment = false;
    private Long id;
}
