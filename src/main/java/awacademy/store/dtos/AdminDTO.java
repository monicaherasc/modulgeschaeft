package awacademy.store.dtos;

import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
public class AdminDTO {

    private String businessName;
    private String password;
    private String street;
    private String postcode;
    private String city;
    private String country;
    private String phoneNumber;
    // email = username in auth
    private String email;
    private String branche;
    private Timestamp timeStamp;
    private boolean onlinePayment;
    private String category;

    private Long id;

}
