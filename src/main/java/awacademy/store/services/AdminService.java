package awacademy.store.services;

import awacademy.store.dtos.AdminDTO;
import awacademy.store.entities.BusinessEntity;
import awacademy.store.respositories.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {


    private BusinessRepository businessRepository;

    @Autowired
    public AdminService(BusinessRepository companyRepository) {
        this.businessRepository =companyRepository;
    }


    /**
     * Modul Verwaltung wünscht sich eine Liste von Businesses mit Zeitstempel
     * @return list of businesses
     */
    public List<AdminDTO> readAllCompanies(String startDate, String endDate) {

        List<AdminDTO> adminDTOList = new ArrayList<>();

        List<BusinessEntity> businessEntityList = businessRepository.findByTimeStamp(startDate,endDate);

        for (BusinessEntity businessEntity : businessEntityList) {
            adminDTOList.add(convertModel(businessEntity));
        }
        return adminDTOList;
    }



    public AdminDTO convertModel(BusinessEntity businessEntity) {

        AdminDTO adminDTO = new AdminDTO();

        adminDTO.setStreet(businessEntity.getStreet());
        adminDTO.setCity(businessEntity.getCity());
        adminDTO.setCountry(businessEntity.getCountry());
        adminDTO.setPhoneNumber(businessEntity.getPhoneNumber());
        adminDTO.setPostcode(businessEntity.getPostcode());
        adminDTO.setStreet(businessEntity.getStreet());
        adminDTO.setOnlinePayment(businessEntity.isOnlinePayment());
        adminDTO.setBranche(businessEntity.getBranche());
        adminDTO.setEmail(businessEntity.getEmail());
        adminDTO.setBusinessName(businessEntity.getBusinessName());

        //ToDo - mögliche Fehler Quelle - im Auge behalten (Formatierung von TimeStamp zu String etc.)
        adminDTO.setTimeStamp(businessEntity.getTimeStamp());

        return adminDTO;

    }


    //TODo - Not needed
//    public BusinessEntity convert(AdminDTO adminDTO) {
//
//        BusinessEntity businessEntity = new BusinessEntity();
//
//        businessEntity.setBusinessName(adminDTO.getBusinessName());
//        businessEntity.setPassword(adminDTO.getPassword());
//        businessEntity.setStreet(adminDTO.getStreet());
//        businessEntity.setBranche(adminDTO.getBranche());
//        businessEntity.setEmail(adminDTO.getEmail());
//        businessEntity.setPhoneNumber(adminDTO.getPhoneNumber());
//        businessEntity.setCity(adminDTO.getCity());
//        businessEntity.setCountry(adminDTO.getCountry());
//        businessEntity.setPassword(adminDTO.getPassword());
//        businessEntity.setPostcode(adminDTO.getPostcode());
//        businessEntity.setOnlinePayment(adminDTO.isOnlinePayment());
//
//        return businessEntity;
//    }




//        public List<AdminDTO> readAllCompanies() {
//            List<BusinessEntity> lista = businessRepository.findAll();
//            List<AdminDTO> dto = convertModel(lista);
//            return dto;
//
//        }



//        public List<AdminDTO> convertModel(List<BusinessEntity> all) {
//            List<AdminDTO> dtos = new ArrayList<>();
//
//            for (BusinessEntity companyEntity : all) {
//                AdminDTO dtoDto = new AdminDTO();
//                dtoDto.setName(companyEntity.getName());
//                dtoDto.setPassword(companyEntity.getPassword());
//                dtoDto.setAddress(companyEntity.getAddress());
//                dtoDto.setCategory(companyEntity.getCategory());
//                dtoDto.setMail(companyEntity.getMail());
//                dtoDto.setPhoneNumber(companyEntity.getPhoneNumber());
//                dtoDto.setId(companyEntity.getId());
//
//                dtos.add(dtoDto);
//            }
//            return dtos;
//        }


    }

