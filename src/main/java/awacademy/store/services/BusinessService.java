package awacademy.store.services;

import awacademy.store.dtos.BusinessDTO;
import awacademy.store.entities.BusinessEntity;
import awacademy.store.respositories.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusinessService {


    private BusinessRepository businessRepository;

    @Autowired
    public BusinessService(BusinessRepository businessRepository) {
        this.businessRepository = businessRepository;
    }



    public List<BusinessDTO> readAllBusiness() {

        List<BusinessDTO> businessDTOList = new ArrayList<>();
        List<BusinessEntity> businessEntityList =  businessRepository.findAll();

        for (BusinessEntity businessEntity : businessEntityList) {
            businessDTOList.add(convertModel(businessEntity));
        }
        return businessDTOList;
    }

    public void createNewBusiness(BusinessDTO businessDto){

        // Die Frage ist, wird hier bereits eine Business ID generiert?
        BusinessEntity businessEntity = convert(businessDto);

        //Timestamp - mainly needed for AdminModule
        LocalDateTime localDateTime = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(localDateTime);
        businessEntity.setTimeStamp(timestamp);

        businessRepository.save(businessEntity);


    }

    public BusinessEntity readByID(Long id) {
        return businessRepository.findById(id).get();
    }

    public BusinessEntity convert (BusinessDTO businessDto){

        BusinessEntity businessEntity = new BusinessEntity();

        businessEntity.setBusinessName(businessDto.getBusinessName());
        businessEntity.setPassword(businessDto.getPassword());
        businessEntity.setStreet(businessDto.getStreet());
        businessEntity.setBranche(businessDto.getBranche());
        businessEntity.setEmail(businessDto.getEmail());
        businessEntity.setPhoneNumber(businessDto.getPhoneNumber());
        businessEntity.setCity(businessDto.getCity());
        businessEntity.setCountry(businessDto.getCountry());
        businessEntity.setPassword(businessDto.getPassword());
        businessEntity.setPostcode(businessDto.getPostcode());
        businessEntity.setOnlinePayment(businessDto.isOnlinePayment());
        //mappen of qrCode
        businessEntity.setQrCode(businessDto.getQrCode());

        return businessEntity;
    }

    public BusinessDTO convertModel(BusinessEntity businessEntity) {

        BusinessDTO businessDto = new BusinessDTO();
        businessDto.setStreet(businessEntity.getStreet());
        businessDto.setCity(businessEntity.getCity());
        businessDto.setCountry(businessEntity.getCountry());
        businessDto.setPhoneNumber(businessEntity.getPhoneNumber());
        businessDto.setPostcode(businessEntity.getPostcode());
        businessDto.setStreet(businessEntity.getStreet());
        businessDto.setOnlinePayment(businessEntity.isOnlinePayment());
        businessDto.setBranche(businessEntity.getBranche());
        businessDto.setEmail(businessEntity.getEmail());
        businessDto.setBusinessName(businessEntity.getBusinessName());
        //BusinessModel erhält ID aus DB
        //ToDo - This might not be up to Date!!
        businessDto.setId(businessEntity.getId());
        businessDto.setQrCode(businessEntity.getQrCode());

        return businessDto;

    }

    @Transactional
    public void delete(Long id) {

       BusinessEntity businessEntity = findById(id);

        if (businessEntity!= null){
        businessRepository.deleteById(id);
        }else {
            System.out.println("The user doesnt exists");
        }

    }


    @Transactional(readOnly = true)
    public BusinessEntity findById(Long id) {
        return businessRepository.findById(id).orElse(null);
    }


    @Transactional
    public BusinessEntity save(BusinessEntity businessEntity) {

        return businessRepository.save(businessEntity);
    }


    public BusinessEntity update (BusinessDTO businessDTO, Long id){
        BusinessEntity clienteActual = findById(id);
        clienteActual.setBusinessName(businessDTO.getBusinessName());
        clienteActual.setCity(businessDTO.getCity());
        clienteActual.setCountry(businessDTO.getCountry());
        clienteActual.setBranche(businessDTO.getBranche());
        clienteActual.setEmail(businessDTO.getEmail());
        clienteActual.setOnlinePayment(businessDTO.isOnlinePayment());
        clienteActual.setPhoneNumber(businessDTO.getPhoneNumber());
        return clienteActual;
    }






}
