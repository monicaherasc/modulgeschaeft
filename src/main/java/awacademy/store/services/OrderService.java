package awacademy.store.services;

import awacademy.store.dtos.OrderDTO;
import awacademy.store.dtos.ess.EssOrderDTO;
import awacademy.store.entities.OfferEntity;
import awacademy.store.entities.OrderEntity;
import awacademy.store.respositories.OfferRepository;
import awacademy.store.respositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {


    private OrderRepository orderRepository;
    private OfferRepository offerRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, OfferRepository offerRepository) {
        this.orderRepository = orderRepository;
        this.offerRepository = offerRepository;
    }

    /**
     * This takes the response Code from ESS and looks for the Offer in DB, then it
     * maps all the needed info of the offer (title, description) and saves it as an
     * order to the DB. Later we can simply read our orders from DB.
     * @param essOrderDTO
     */
    public void createOrder(EssOrderDTO essOrderDTO) {

        // Reads the required Offer (from api response code)
        OfferEntity offerEntity = offerRepository.findOrderByCode(essOrderDTO.getCode());

        // maps it to order entity and saves it in DB
        orderRepository.save(mapToOrder(offerEntity));

    }

    public List<OrderDTO> readAllOrders() {
        List<OrderDTO> orderDTOS = new ArrayList<>();
        List<OrderEntity> orderEntities = orderRepository.findAll();
        orderDTOS = convertToModel(orderEntities);

//        List<OrderDTO> orderDTOS = new ArrayList<>();
//
//        List<OrderEntity> orderEntities = orderRepository.findAll();
//        for (OrderEntity entity : orderEntities) {
//            orderDTOS.add(convertModel(entity));
//        }
        return orderDTOS;
    }


    private OrderEntity mapToOrder(OfferEntity offerEntity) {

        OrderEntity newOrder = new OrderEntity();
        newOrder.setDescription(offerEntity.getPrice());
        newOrder.setTitle(offerEntity.getTitle());
        newOrder.setId(offerEntity.getId());
        newOrder.setCustomerID(offerEntity.getCustomerID());

        // we get a DateTime
        LocalDateTime dateTime = LocalDateTime.now();
        String orderDate = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        newOrder.setDateOfOrder(orderDate);

        return newOrder;

    }

    /**
     * Maps to Model
     *
     * @param order give it a entity
     * @return returns a model
     */
    private List<OrderDTO> convertToModel(List<OrderEntity> orderList) {
        List<OrderDTO> listDto = new ArrayList<>();

        for(OrderEntity orderEntity: orderList) {
            OrderDTO orderDto= new OrderDTO();
            orderDto.setCustomerID(orderEntity.getCustomerID());
            orderDto.setDescription(orderEntity.getDescription());
            orderDto.setTitle(orderEntity.getTitle());
            orderDto.setId(orderEntity.getId());
            orderDto.setDateOfOrder(orderEntity.getDateOfOrder());

            listDto.add(orderDto);
        }

        return listDto;
    }


}
