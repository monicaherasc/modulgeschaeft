package awacademy.store.services;



import awacademy.store.dtos.OfferDTO;
import awacademy.store.dtos.OfferToSendDTO;

import awacademy.store.entities.OfferEntity;
import awacademy.store.respositories.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class OfferService {


    private OfferRepository offerRepository;

    @Autowired
    public OfferService(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }



    public void createOffer(OfferToSendDTO offerToSendDTO){


        offerRepository.save(convert(offerToSendDTO));
    }

    public List<OfferDTO> readAllOffer() {
        List<OfferDTO> offerDTOS = new ArrayList<>();
        List<OfferEntity> offerEntities = offerRepository.findAll();
        offerDTOS = convertToModel(offerEntities);


//        for (OfferEntity offer : offerEntities) {
//
//            offerDTOS.add(converToModel(offer));
//        }
        return offerDTOS;
    }


    //ToDo - delete needs to get fixed
    public void delete(OfferDTO offerDTO) {

        // Without Optional this casting could give a NullPointerException
        OfferEntity offer = offerRepository.findById(offerDTO.getId()).get();

        offerRepository.delete(offer);
    }




    /**
     * Mapping to model
     * @param offer give it a entity
     * @return returns a model
     */
    private List<OfferDTO> convertToModel(List<OfferEntity> offerList) {
        List<OfferDTO> listDto = new ArrayList<>();

        for(OfferEntity offerEntity: offerList) {
            OfferDTO offerDto= new OfferDTO();

            offerDto.setAbout(offerEntity.getAbout());
            offerDto.setPrice(offerEntity.getPrice());
            offerDto.setId(offerEntity.getId());
            offerDto.setTitle(offerEntity.getTitle());
            offerDto.setCode(offerEntity.getCode());

            listDto.add(offerDto);
        }

        return listDto;
    }


    /**
     * Mapping to entity
     * @param offerDTO give it a entity
     * @return returns a model
     */
    private OfferEntity convert(OfferToSendDTO offerToSendDTO) {

        OfferEntity newOfferEntity = new OfferEntity();

        newOfferEntity.setAbout(offerToSendDTO.getAbout());
        newOfferEntity.setPrice(offerToSendDTO.getPrice());
        newOfferEntity.setTitle(offerToSendDTO.getTitle());
        newOfferEntity.setCode(offerToSendDTO.getCode());
        newOfferEntity.setBusinessId(offerToSendDTO.getBusinessId());

        return newOfferEntity;
    }

    @Transactional
    public void delete(Long id) {

        OfferEntity offerEntity = findById(id);

        if (offerEntity!= null){
            offerRepository.deleteById(id);
        }else {
            System.out.println("No user found");
        }

    }

    @Transactional(readOnly = true)
    public OfferEntity findById(Long id) {
        return offerRepository.findById(id).orElse(null);
    }

    @Transactional
    public OfferEntity save(OfferEntity offerEntity) {

        return offerRepository.save(offerEntity);
    }

    public OfferEntity update (OfferDTO offerDTO, Long id){
        OfferEntity clienteActual = findById(id);
        clienteActual.setAbout(offerDTO.getAbout());
        clienteActual.setPrice(offerDTO.getPrice());
        clienteActual.setTitle(offerDTO.getTitle());

        return clienteActual;
    }
}
