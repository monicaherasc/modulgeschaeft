package awacademy.store.services;

import awacademy.store.dtos.BusinessIdDTO;

import awacademy.store.dtos.OfferToSendDTO;
import awacademy.store.dtos.ess.OcodeDTO;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
public class ExternalServiceService {

    private final RestTemplate restTemplate;

    @Autowired
    public ExternalServiceService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }



    //There is a good reason why we use a Long id instead of the whole DTO object within the id
    public String businessRegistration(Long id) {

        final String URL = "https://linkogservices.herokuapp.com/business/registration";

        // We create a DTO so we can send it as a JSON object to the ESS
        BusinessIdDTO businessIdDTO = new BusinessIdDTO();
        businessIdDTO.setBusinessID(id);


        //RestTemplate aufrufen und code als String zurückbekommen
        String businessQRcode = restTemplate.postForObject(URL, businessIdDTO, String.class);

        return businessQRcode;
    }

    public OfferToSendDTO offerRegistration(OfferToSendDTO offerToSendDTO) {

        final String URL = "https://linkogservices.herokuapp.com/business/offer";
        String code = UUID.randomUUID().toString() + "O";

        OcodeDTO ocodeDTO = new OcodeDTO();

        ocodeDTO.setCode(code);
        ocodeDTO.setId(offerToSendDTO.getBusinessId());

        // if needed in log
        String response = restTemplate.postForObject(URL, ocodeDTO, String.class);
        System.out.println(response);

        offerToSendDTO.setCode(code);

        return offerToSendDTO;
    }


}
