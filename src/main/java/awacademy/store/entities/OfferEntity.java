package awacademy.store.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "offer")
public class OfferEntity {

    private String title;
    private String price;
    private String about;

    @Id
    @GeneratedValue
    private Long id;

    // Generierter QR-String-Code (Mit O)
    private String code;

    // This is null until we map to an Order, so it's just needed as soon an offer is ordered
    private Long customerID;

    private Long businessId;



}
