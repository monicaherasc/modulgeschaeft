package awacademy.store.entities;

import lombok.Data;

import javax.persistence.*;


/**
 * In relation to external service
 */
@Entity
@Data
@Table(name = "order")
public class OrderEntity {



    // We get from ESS
    private Long customerID;


    private String title;

    private String description;

    private String dateOfOrder;



    @Id
    @GeneratedValue
    private Long id;

    // generated code by ESS
    private String code;



}
