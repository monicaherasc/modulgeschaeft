package awacademy.store.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "qrcode")
public class CodeEntity {

    @Id
    private Long id;
}
