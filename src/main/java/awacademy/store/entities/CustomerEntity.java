package awacademy.store.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This entity might be needed as a primary key to persist our anonymous connection with the external service
 */

@Entity
@Data
@Table(name = "customer")
public class CustomerEntity {

    @Id
    @GeneratedValue
    private Long id; // We might use this instead of the customerID in OrderEntity

    // ToDo - Weitere Attribute noch in Absprache mit dem External Service Team
}
