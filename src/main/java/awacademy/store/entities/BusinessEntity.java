package awacademy.store.entities;


import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "businessTable")
public class BusinessEntity {

    // Ein Unternehmen hat 1 - n Aufträge
    @OneToMany
    List<OrderEntity> orderEntities;

    // Ein Unternehmen hat 1 - n Angebote
    @OneToMany
    List<OfferEntity> offerEntities;

    @Id
    @GeneratedValue
    private Long id;

    // Generated in ESS-Controller
    private String qrCode;

    private String businessName;
    private String password;
    private String street;
    private String postcode;
    private String city;
    private String country;
    private String phoneNumber;
    // email = username in auth
    private String email;
    private String branche;
    private Timestamp timeStamp;
    private boolean onlinePayment;

}
